#!/bin/bash
#
# Copyright (c) 2021, Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#

OS="${OS:-ubuntu}"
OS_VER="${OS_VERSION:-bionic}"
MODEL_DIR="${MODEL_DIR:-/opt/model}"

function model() {
tgz=$1
arr_model=(${tgz//./ })
x=${arr_model[0]##*_}
y=${arr_model[1]}
z=${arr_model[2]}
if [ -n "$z" -a "$z" != "tgz" ]; then
		MODEL_VER="${x}.${y}.${z}"
else
		MODEL_VER="${x}.${y}"
fi
MODEL=$(echo $tgz | sed "s/_${MODEL_VER}.tgz//")
}

target_dir=$1
tgz=$2

# get MODEL and MODEL_VER
model $tgz

# replace template with real values
sed -e "s|\${OS}|${OS}|g" \
    -e "s|\${OS_VER}|${OS_VER}|g" \
    -e "s|\${MODEL}|${MODEL}|g" \
    -e "s|\${MODEL_VER}|${MODEL_VER}|g" \
    -e "s|\${MODEL_DIR}|${MODEL_DIR}|g" < dockerfile-template > $target_dir/Dockerfile
